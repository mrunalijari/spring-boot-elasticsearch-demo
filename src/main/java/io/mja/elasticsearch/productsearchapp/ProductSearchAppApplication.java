package io.mja.elasticsearch.productsearchapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductSearchAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductSearchAppApplication.class, args);
	}

}
