package io.mja.elasticsearch.productsearchapp.controller;

import io.mja.elasticsearch.productsearchapp.model.IndexStructureModel;
import io.mja.elasticsearch.productsearchapp.services.DynamicIndexManagementServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/dynamic-indexes")
public class DynamicIndexManagementController {

    private final DynamicIndexManagementServiceImpl dynamicIndexManagementService;

    @Autowired
    public DynamicIndexManagementController(DynamicIndexManagementServiceImpl dynamicIndexManagementService) {
        this.dynamicIndexManagementService = dynamicIndexManagementService;
    }

    @PostMapping
    public ResponseEntity<String> createIndex(@RequestBody IndexStructureModel request) throws IOException {
        dynamicIndexManagementService.createIndex(request);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body("SUCCESS");
    }

    @DeleteMapping("/{indexName}")
    public ResponseEntity<String> deleteIndex(@PathVariable String indexName) throws IOException {
        dynamicIndexManagementService.deleteIndex(indexName);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body("SUCCESS");
    }

    @PutMapping("/{indexName}/{destIndexName}")
    public ResponseEntity<String> reIndexing(@PathVariable String indexName, @PathVariable String destIndexName, @RequestParam String removeField) throws IOException, InterruptedException {
        dynamicIndexManagementService.reIndexing(indexName, destIndexName, removeField);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body("SUCCESS");
    }
}
