package io.mja.elasticsearch.productsearchapp.controller;

import io.mja.elasticsearch.productsearchapp.entities.Product;
import io.mja.elasticsearch.productsearchapp.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    /*@PostMapping("/with-repo")
    public ResponseEntity<String> createProductIndexWithRepo(@RequestBody Product product) {
        productService.createProductIndexWithRepo(product);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body("SUCCESS");
    }*/

    @PostMapping
    public ResponseEntity<String> createProductIndex(@RequestParam String indexName, @RequestBody Product product) throws IOException {
        String id = productService.createProductIndex(indexName, product);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProductIndex(@RequestParam String indexName, @PathVariable String id) {
        productService.deleteProductIndex(indexName, id);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body("SUCCESS");
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findProductById(@RequestParam String indexName, @PathVariable String id) {
        Product product = productService.findProductById(indexName, id);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body(product);
    }

    @GetMapping("/search/{brandName}")
    public ResponseEntity<List<Product>> findProductsByBrand(@RequestParam String indexName, @PathVariable String brandName) {
        List<Product> products = productService.findProductsByBrand(indexName, brandName);
        return ResponseEntity.ok().cacheControl(CacheControl.noCache()).body(products);
    }
}
