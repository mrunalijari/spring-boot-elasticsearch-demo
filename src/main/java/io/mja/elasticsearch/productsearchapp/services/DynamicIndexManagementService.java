package io.mja.elasticsearch.productsearchapp.services;

import io.mja.elasticsearch.productsearchapp.model.IndexStructureModel;

import java.io.IOException;

public interface DynamicIndexManagementService {

    void createIndex(IndexStructureModel request) throws IOException;

    void deleteIndex(String indexName) throws IOException;

    void reIndexing(String indexName, String destIndexName, String removeField) throws IOException, InterruptedException;
}
