package io.mja.elasticsearch.productsearchapp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.mja.elasticsearch.productsearchapp.entities.Product;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    /*private final ProductRepository productRepository;*/
    private final ElasticsearchOperations elasticsearchOperations;
    private final RestHighLevelClient restHighLevelClient;

    @Autowired
    public ProductServiceImpl(/*ProductRepository productRepository,*/ ElasticsearchOperations elasticsearchOperations, RestHighLevelClient restHighLevelClient) {
        /*this.productRepository = productRepository;*/
        this.elasticsearchOperations = elasticsearchOperations;
        this.restHighLevelClient = restHighLevelClient;
    }

    @Override
    public void createProductIndexWithRepo(Product product) {
        //productRepository.save(product);
    }

    @Override
    public String createProductIndex(String indexName, Product product) throws IOException {
        IndexRequest indexRequest = new IndexRequest(indexName);
        indexRequest.id(UUID.randomUUID().toString());
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(product);
        indexRequest.source(json, XContentType.JSON);
        IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        return response.getId();
    }

    @Override
    public void deleteProductIndex(String indexName, String id) {
        Product product = findProductById(indexName, id);
        elasticsearchOperations.delete(product, IndexCoordinates.of(indexName));
    }

    @Override
    public Product findProductById(String indexName, String id) {
        QueryBuilder queryBuilder = QueryBuilders.matchQuery("_id", id);

        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .build();

        SearchHits<Product> productHits =
                elasticsearchOperations
                        .search(searchQuery, Product.class,
                                IndexCoordinates.of(indexName));


        if (productHits.getSearchHits().isEmpty()) {
            return null;
        }

        return productHits.getSearchHits().get(0).getContent();
    }

    @Override
    public List<Product> findProductsByBrand(String indexName, final String brandName) {
        QueryBuilder queryBuilder = QueryBuilders
                .matchQuery("manufacturer", brandName);

        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .build();

        SearchHits<Product> productHits =
                elasticsearchOperations
                        .search(searchQuery, Product.class,
                                IndexCoordinates.of(indexName));

        log.info("productHits {} {}", productHits.getSearchHits().size(), productHits.getSearchHits());

        List<SearchHit<Product>> searchHits =
                productHits.getSearchHits();

        List<Product> products = new ArrayList<>();

        for (SearchHit<Product> searchHit : searchHits) {
            products.add(searchHit.getContent());
        }

        return products;
    }
}
