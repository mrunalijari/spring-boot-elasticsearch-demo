package io.mja.elasticsearch.productsearchapp.services;

import io.mja.elasticsearch.productsearchapp.model.FieldStructureModel;
import io.mja.elasticsearch.productsearchapp.model.IndexStructureModel;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.*;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.ReindexRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public class DynamicIndexManagementServiceImpl implements DynamicIndexManagementService {

    private final RestHighLevelClient restHighLevelClient;

    @Autowired
    public DynamicIndexManagementServiceImpl(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    @Override
    public void createIndex(IndexStructureModel request) throws IOException {
        String indexName = request.getIndexName();

        GetIndexRequest getIndex = new GetIndexRequest(indexName);
        if (!restHighLevelClient.indices().exists(getIndex, RequestOptions.DEFAULT)) {

            CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            {
                builder.field("dynamic","strict");
                builder.startObject("properties");
                {
                    for (FieldStructureModel field :request.getFields()) {
                        builder.startObject(field.getName());
                        {
                            switch (field.getDataType()) {
                                case "string":
                                    builder.field("type", "text");
                                    break;
                                case "integer":
                                    builder.field("type", "integer");
                                    break;
                                case "double":
                                    builder.field("type", "double");
                                    break;
                            }
                        }
                        builder.endObject();
                    }
                }
                builder.endObject();
            }
            builder.endObject();
            createIndexRequest.mapping(builder);
            CreateIndexResponse response = restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
            response.index();
        }
    }

    @Override
    public void deleteIndex(String indexName) throws IOException {
        GetIndexRequest getIndex = new GetIndexRequest(indexName);
        if (restHighLevelClient.indices().exists(getIndex, RequestOptions.DEFAULT)) {
            DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
            restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
        }
    }

    public void reIndexing(String indexName, String destIndexName, String removeField) throws IOException, InterruptedException {
        GetIndexRequest getIndex = new GetIndexRequest(indexName);
        if (restHighLevelClient.indices().exists(getIndex, RequestOptions.DEFAULT)) {

            //Update index
            UpdateByQueryRequest request = new UpdateByQueryRequest(indexName);
            request.setConflicts("proceed");

            Script script = null;
            if (removeField.contains(".")) {
                String parentField = removeField.substring(0, removeField.lastIndexOf('.'));
                String childField = removeField.substring(removeField.lastIndexOf('.') + 1);
                script = new Script("ctx._source." + parentField + ".remove('" + childField + "')");
            } else {
                script = new Script("ctx._source.remove('" + removeField + "')");
            }
            request.setScript(script);

            restHighLevelClient.updateByQuery(request, RequestOptions.DEFAULT);

            // Get structure of index
            /*GetMappingsRequest getMappingsRequest = new GetMappingsRequest();
            getMappingsRequest.indices(indexName);
            GetMappingsResponse getMappingsResponse = restHighLevelClient.indices().getMapping(getMappingsRequest, RequestOptions.DEFAULT);
            Map<String, Object> sourceMap = getMappingsResponse.mappings().get(indexName).getSourceAsMap();
            Map<String, Object> propertiesMap = (Map<String, Object>) sourceMap.get("properties");
            propertiesMap.remove(removeField);

            CreateIndexRequest createIndexRequest = new CreateIndexRequest(destIndexName);
            createIndexRequest.mapping(sourceMap);
            restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);

            Thread.sleep(2000);

            // Reindexing
            ReindexRequest reindexRequest = new ReindexRequest();
            reindexRequest.setSourceIndices(indexName);
            reindexRequest.setDestIndex(destIndexName);

            restHighLevelClient.reindex(reindexRequest, RequestOptions.DEFAULT);

            Thread.sleep(2000);

            deleteIndex(indexName);*/
        }
    }
}
