package io.mja.elasticsearch.productsearchapp.services;

import io.mja.elasticsearch.productsearchapp.entities.Product;

import java.io.IOException;
import java.util.List;

public interface ProductService {

    void createProductIndexWithRepo(Product product);

    String createProductIndex(String indexName, Product product) throws IOException;

    void deleteProductIndex(String indexName, String id);

    Product findProductById(String indexName, String id);

    List<Product> findProductsByBrand(String indexName, final String brandName);
}
