package io.mja.elasticsearch.productsearchapp.model;

import java.util.ArrayList;
import java.util.List;

public class IndexStructureModel {

    private String indexName;
    private List<FieldStructureModel> fields = new ArrayList<>();

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public List<FieldStructureModel> getFields() {
        return fields;
    }

    public void setFields(List<FieldStructureModel> fields) {
        this.fields = fields;
    }
}
