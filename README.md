# ElasticsearchSpringBootDemo

It is important to run elasticsearch before running this project.

Run Project with below commands
1. mvnw clean
2. mvnw install
3. mvnw spring-boot:run

Default Port: 8080
Swagger url: http://localhost:8080/swagger-ui.html

Request for create index API:

`POST - localhost:8080/dynamic-indexes`

Note: "db-jooq" is schema name

Request for create index API:

`POST - localhost:8080/dynamic-indexes`

Request Payload:

`{
      "indexName": "productindex",
      "fields": [
          {
              "name": "id",
              "dataType": "string"
          },
          {
              "name": "name",
              "dataType": "string"
          },
          {
              "name": "price",
              "dataType": "double"
          },
          {
              "name": "quantity",
              "dataType": "integer"
          },
          {
 			"name":"description",
 			"dataType": "string"
 		 },
 		 {
 			"name":"manufacturer",
 			"dataType": "string"
 		 }
      ]
  }`
  
Request for delete index API:
  
`DELETE - localhost:8080/dynamic-indexes/{indexName}`
  
Request to check if index is created or not:
 
`GET - localhost:9200/productindex`
 
 Request for create product API:
 
`POST - localhost:8080/products?indexName={index-name}`

Request Payload:

`{
     "name": "prod1",
     "price": 100.5,
     "quantity": 1,
     "description": "Test product",
     "manufacturer": "mja"
 }`
 
 Request to get product by id API:
 
 `GET - localhost:8080/products/{id}?indexName={index-name}`
 
 Request to delete product by id API:
 
 `DELETE - localhost:8080/products/{id}?indexName={index-name}`
 
 Request for reindexing API
 
 `localhost:8080/dynamic-indexes/{indexName}/{destIndexName}?removeField={remove-filed-name}`
 